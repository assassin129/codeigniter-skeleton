<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	protected $_data;

	public function __construct()
	{
		parent::__construct();
		$this->_data['module'] = $this->router->fetch_module();
		$this->_data['controller'] = $this->router->fetch_class();		
		$this->_data['action'] = $this->router->fetch_method();
		$this->_data['path'] = $this->_data['module'].'/template';
	}

}

/* End of file Admin_Controller.php */
/* Location: ./application/core/Admin_Controller.php */