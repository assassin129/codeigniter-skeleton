<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Controller test hoạt động của CI 3.0.0 - HMCV
* Cấu hình
* Xóa index_page = index.php
* Xóa index bằng htaccess
* Cấu hình sẵn:
* - Admin controller
* - From validation
* - Helper Create slug: chuyển title sang tiếng việt không dấu
* - Helper Create GUID: sinh chuỗi không trùng
* - Config thêm .html vào cuối link
* - Thư mục public: chứa template
* - Thư mục third_party: chứa các phần mở rộng
* - Master layout
* - Admin layout home, auth
* - Autoload helper url
* - RESTfull server
**/
class Test extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(TRUE);
	}
	public function index()
	{
		$this->_data['title'] = 'Admin test tool';
		$this->_data['page_load'] = 'test/index_view';
		$this->load->view($this->_data['path'], $this->_data);
	}

}

/* End of file test.php */
/* Location: ./application/modules/admin/controllers/test.php */