<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">About</div>
				<div class="panel-body">
					<code>
						<ul>
							<li> Controller test hoạt động của CI 3.0.0 - HMVC</li>
							<li> Cấu hình</li>
							<li> Xóa index_page = index.php</li>
							<li> Xóa index bằng htaccess</li>
							<li> Cấu hình sẵn:</li>
							<ul>
								<li> Admin controller</li>
								<li> Form validation</li>
								<li> Helper Create slug: chuyển title sang tiếng việt không dấu</li>
								<li> Helper Create GUID: sinh chuỗi không trùng</li>
								<li> Config thêm .html vào cuối link</li>
								<li> Thư mục public: chứa template</li>
								<li> Thư mục third_party: chứa các phần mở rộng</li>
								<li> Master layout</li>
								<li> Admin layout home, auth</li>
								<li> Autoload helper url</li>
								<li> RESTfull server</li>
							</ul>
						</ul>
					</code>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "site_url"; ?></div>
				<div class="panel-body"><?php echo site_url(); ?></div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "current_url"; ?></div>
				<div class="panel-body"><?php echo current_url(); ?></div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "base_url"; ?></div>
				<div class="panel-body"><?php echo base_url(); ?></div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "create_slug"; ?></div>
				<div class="panel-body">
					<p>ký tự ( “ ) vẫn như vậy</p>
					<p><?php echo create_slug('ký tự ( “ ) vẫn như vậy'); ?></p>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "create_guid"; ?></div>
				<div class="panel-body"><?php echo create_guid(); ?></div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo "RESTfull server"; ?></div>
				<div class="panel-body"><a target="_blank" href="<?php echo site_url('api/example/users'); ?>">Get users</a></div>
			</div>
			<div class="panel panel-danger">
				<div class="panel-heading"><?php echo "Important"; ?></div>
				<div class="panel-body">Reference code style: <a target="_blank" href="http://www.codeigniter.com/user_guide/general/styleguide.html">http://www.codeigniter.com/user_guide/general/styleguide.html</a></div>
			</div>
		</div>
	</div>
</div>
